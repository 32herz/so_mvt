package com.zzz.simulador;

import java.awt.Dimension;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.GroupLayout.Alignment;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.UIManager;

public class GUI {

	private GroupLayout panelGroupLayout;
	private JButton botonControlSimulacion;
	private JFrame ventana;
	private JLabel etiquetaTitulo;
	private JLabel etiquetaEstado;
	private JPanel panelGrafica;
	private JScrollPane panelProcesos;
	private JScrollPane panelAreasLibres;
	private JScrollPane panelParticiones;
	private JTable tablaProcesos;
	private JTable tablaAreasLibres;
	private JTable tablaParticiones;

	public GUI() {
		inicializacion();
	}

	public JButton getBotonControlSimulacion() {
		return this.botonControlSimulacion;
	}

	public JTable getTablaProcesos() {
		return this.tablaProcesos;
	}

	public JTable getTablaAreaLibre() {
		return this.tablaAreasLibres;
	}

	public JTable getTablaParticiones() {
		return this.tablaParticiones;
	}

	public JPanel getPanelGrafica() {
		return this.panelGrafica;
	}

	public void mostrarVentana() {
		ventana.setVisible(true);
	}

	private void inicializacion() {
		
		UIManager.put("swing.boldMetal", Boolean.FALSE);
		UIManager.getDefaults().put("TableHeader.cellBorder" , BorderFactory.createEmptyBorder(0,0,0,0));

		ventana = new JFrame("Simulación MVT");

		etiquetaTitulo = new JLabel("SIMULACIÓN DE ASIGNACIÓN DE MEMORIA CON MVT");
		etiquetaTitulo.setFont(etiquetaTitulo.getFont().deriveFont(18.0F).deriveFont(Font.BOLD));

		tablaProcesos = new CustomTable();
		tablaAreasLibres = new CustomTable();
		tablaParticiones = new CustomTable();

		panelProcesos = new JScrollPane(tablaProcesos);
		panelProcesos.setBackground(Color.WHITE);
		panelProcesos.setBorder(BorderFactory.createTitledBorder("Procesos"));

		panelAreasLibres = new JScrollPane(tablaAreasLibres);
		panelAreasLibres.setBorder(BorderFactory.createTitledBorder("Área libre"));
		panelAreasLibres.setBackground(Color.WHITE);

		panelParticiones = new JScrollPane(tablaParticiones);
		panelParticiones.setBorder(BorderFactory.createTitledBorder("Particiones"));
		panelParticiones.setBackground(Color.WHITE);

		panelGrafica = new JPanel();
		panelGrafica.setBorder(BorderFactory.createTitledBorder("Representación gráfica"));
		panelGrafica.setBackground(Color.WHITE);

		botonControlSimulacion = new JButton("Paso: 0");

		etiquetaEstado = new JLabel();

		panelGroupLayout = new GroupLayout(ventana.getContentPane());
		ventana.getContentPane().setLayout(panelGroupLayout);

		panelGroupLayout.setAutoCreateGaps(true);
		panelGroupLayout.setAutoCreateContainerGaps(true);

		panelGroupLayout.setHorizontalGroup(
				panelGroupLayout.createParallelGroup(Alignment.CENTER)
					.addComponent(etiquetaTitulo)
					.addComponent(panelProcesos, GroupLayout.PREFERRED_SIZE, 760, GroupLayout.PREFERRED_SIZE)
					.addGroup(panelGroupLayout.createSequentialGroup()
						.addGroup(panelGroupLayout.createParallelGroup()
							.addComponent(panelAreasLibres, GroupLayout.PREFERRED_SIZE, 380, GroupLayout.PREFERRED_SIZE)
							.addComponent(panelParticiones, GroupLayout.PREFERRED_SIZE, 380, GroupLayout.PREFERRED_SIZE))
						.addComponent(panelGrafica, GroupLayout.PREFERRED_SIZE, 380, GroupLayout.PREFERRED_SIZE))
					.addComponent(botonControlSimulacion)
					.addComponent(etiquetaEstado)
				);
		panelGroupLayout.setVerticalGroup(
				panelGroupLayout.createSequentialGroup()
					.addComponent(etiquetaTitulo)
					.addComponent(panelProcesos, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
					.addGroup(panelGroupLayout.createParallelGroup()
						.addGroup(panelGroupLayout.createSequentialGroup()
							.addComponent(panelAreasLibres, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
							.addComponent(panelParticiones, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE))
						.addComponent(panelGrafica, GroupLayout.PREFERRED_SIZE, 466, GroupLayout.PREFERRED_SIZE))
					.addComponent(botonControlSimulacion)
					.addComponent(etiquetaEstado)
				);

		ventana.getContentPane().setBackground(Color.WHITE);
		ventana.pack();
		ventana.setResizable(false);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	private class CustomTable extends JTable {

		public CustomTable() {
			initTable();
		}

		private void initTable() {

			setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
				@Override
				public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {
					JLabel label  = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
					
					if (!isRowSelected(row)) {
						label.setBackground(row % 2 == 0 ? Color.WHITE : new Color(199, 241, 224));
					}
					
					label.setHorizontalAlignment(SwingConstants.CENTER);
					return label;
				}
			});
			setFillsViewportHeight(true);
			setEnabled(false);
			setRowHeight(18);
			setRowSelectionAllowed(false);
			setShowGrid(false);
		    getTableHeader().setBackground(new Color(0, 202, 171));
			getTableHeader().setFont(getTableHeader().getFont().deriveFont(Font.BOLD));
			getTableHeader().setForeground(Color.WHITE);
			getTableHeader().setPreferredSize(new Dimension(0, 30));
		
		}

	}

}
