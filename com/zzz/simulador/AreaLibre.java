package com.zzz.simulador;

public class AreaLibre {

	private int localidad;
	private int tamanioArea;
	private Estado estadoArea;

	public AreaLibre(int localidad, int tamanioArea, Estado estadoArea) {
		setLocalidad(localidad);
		setTamanioDeAreaLibre(tamanioArea);
		setEstadoDeAreaLibre(estadoArea);
	}

	public int getLocalidad() {
		return this.localidad;
	}

	public int getTamanioDeAreaLibre() {
		return this.tamanioArea;
	}

	public Estado getEstadoDeAreaLibre() {
		return this.estadoArea;
	}

	public void setLocalidad(int localidad) {
		this.localidad = Math.max(0, localidad);
	}

	public void setTamanioDeAreaLibre(int tamanioArea) {
		this.tamanioArea = Math.max(0, tamanioArea);
	}

	public void setEstadoDeAreaLibre(Estado estadoArea) {
		this.estadoArea = estadoArea;
	}

	@Override
	public String toString() {
		return String.format("[Localidad=%dK, Tamaño=%dK, Estado=%s]",
				getLocalidad(),
				getTamanioDeAreaLibre(),
				getEstadoDeAreaLibre());
	}

}
