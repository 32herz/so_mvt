package com.zzz.simulador;

public class Proceso {

	private String idProceso;
	private int tiempoLlegada;
	private int duracionProceso;
	private int tamanioProceso;
	
	public Proceso(String idProceso, int tamanioProceso, int tiempoLlegada, int duracionProceso) {
		setIdDelProceso(idProceso);
		setTiempoDeLlegada(tiempoLlegada);
		setDuracionDelProceso(duracionProceso);
		setTamanioDelProceso(tamanioProceso);
	}

	public String getIdDelProceso() {
		return this.idProceso;
	}

	public int getTiempoDeLlegada() {
		return this.tiempoLlegada;
	}

	public int getDuracionDelProceso() {
		return this.duracionProceso;
	}

	public int getTamanioDelProceso() {
		return this.tamanioProceso;
	}

	public void setIdDelProceso(String idProceso) {
		this.idProceso = idProceso;
	}

	public void setTiempoDeLlegada(int tiempo) {
		this.tiempoLlegada = Math.max(0, tiempo);	
	}

	public void setDuracionDelProceso(int duracion) {
		this.duracionProceso = Math.max(0, duracion);
	}

	public void setTamanioDelProceso(int tamanio)  {
		this.tamanioProceso = Math.max(0, tamanio);
	}

	@Override
	public String toString() {
		return String.format("[ID: %s, Llegada=%d, Duracion=%dK, Tamaño=%dK]", 
				getIdDelProceso(), 
				getTiempoDeLlegada(), 
				getDuracionDelProceso(), 
				getTamanioDelProceso());
	}

}
