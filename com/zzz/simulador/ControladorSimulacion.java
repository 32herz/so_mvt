package com.zzz.simulador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.JPanel;

class ControladorSimulacion {

	private int paso = 0;
	private int totalMemoria = 64;
	private int totalMemoriaSo = 10;

	private ArrayList<AreaLibre> listaAreaLibre;
	private ArrayList<Particion> listaParticiones;
	private ArrayList<Proceso> listaProcesos;

	private DefaultTableModel modeloProcesos;
	private DefaultTableModel modeloAreaLibre;
	private DefaultTableModel modeloParticiones;
	
	private GUI vista;
	private JButton botonControlSimulacion;
	private JTable tablaProcesos;
	private JTable tablaAreaLibre;
	private JTable tablaParticiones;
	private JPanel panelGrafico;

	private final String[] cabeceraTablaProcesos = {"Proceso", "Tamaño", "Tiempo de llegada", "Duración"};
	private final String[] cabeceraTablaAreaLibre = {"No.", "Localidad", "Tamaño", "Estado"};
	private final String[] cabeceraTablaParticiones = {"No.", "Localidad", "Tamaño", "Estado", "Proceso"};

	public ControladorSimulacion(GUI vista) throws Exception {

		setVista(vista);
		inicializacion();

	}

	public void setVista(GUI vista) throws Exception {

		if (null == vista)
			throw new Exception("Vista  no puede ser null.");

		this.vista = vista;

	}

	public void siguientePasoSimulador() {

		this.paso = this.paso + 1;
		botonControlSimulacion.setText("Paso " + this.paso);

		Proceso procesoActual = listaProcesos.stream().filter(
				t -> paso == t.getTiempoDeLlegada()).findFirst().orElse(null);

		if (null != procesoActual) {
			// Buscamos un area libre en memoria donde pudiese caber el proceso	
			AreaLibre libre = listaAreaLibre
				.stream()
				.filter(p -> procesoActual.getTamanioDelProceso() <= p.getTamanioDeAreaLibre())
				.findFirst()
				.orElse(null);
			
			// Se encontró un area donde insertar el proceso;
			if (null != libre) {
				listaParticiones.add(new Particion(libre.getLocalidad(), 
							procesoActual.getTamanioDelProceso(), 
							Estado.Disponible,
							procesoActual.getIdDelProceso()));

				libre.setLocalidad(libre.getLocalidad() + procesoActual.getTamanioDelProceso());
				libre.setTamanioDeAreaLibre(libre.getTamanioDeAreaLibre() - procesoActual.getTamanioDelProceso());

			} else {
				// TODO: morir
			}

		}

		// Obtenemos, si es que existen, los procesos que terminan en este momento
		List<Proceso> finalizados = listaProcesos
			.stream()
			.filter(t -> t.getDuracionDelProceso() + t.getTiempoDeLlegada() - paso == 0)
			.collect(Collectors.toList());

		if (!finalizados.isEmpty()) {
			// listaAreaLibre.clear();
			for (Proceso proc : finalizados) {
				Particion part = listaParticiones
					.stream()
					.filter(t -> t.getIdDelProceso().equals(proc.getIdDelProceso()))
					.findFirst()
					.orElse(null);

				if (null != part) {
					AreaLibre areaNueva = new AreaLibre(part.getLocalidad(), part.getTamanioDeParticion(), Estado.Disponible);

					ListIterator<AreaLibre> elementos = listaAreaLibre.listIterator();
					while (elementos.hasNext()) {
						AreaLibre actual = elementos.next();
						if (actual.getLocalidad() == areaNueva.getLocalidad() + areaNueva.getTamanioDeAreaLibre()) {
							areaNueva.setTamanioDeAreaLibre(actual.getTamanioDeAreaLibre() + areaNueva.getTamanioDeAreaLibre());
							elementos.remove();
						} else if (areaNueva.getLocalidad() == actual.getLocalidad() + actual.getTamanioDeAreaLibre()) {
							areaNueva.setLocalidad(actual.getLocalidad());
							areaNueva.setTamanioDeAreaLibre(actual.getTamanioDeAreaLibre() + areaNueva.getTamanioDeAreaLibre());
							elementos.remove();
						}
					}
					elementos.add(areaNueva);
					listaAreaLibre.sort((a, b) -> Integer.compare(a.getLocalidad(), b.getLocalidad()));
					listaParticiones.remove(part);
				}
			}
		}

		modeloAreaLibre.setRowCount(0);
		modeloParticiones.setRowCount(0);

		for (int i = 0; i <  listaAreaLibre.size(); i++) {
			AreaLibre area = listaAreaLibre.get(i);
			modeloAreaLibre.addRow(new String[] {Integer.toString(i + 1),
				area.getLocalidad() + "K", 
				area.getTamanioDeAreaLibre() + "K", 
				area.getEstadoDeAreaLibre().toString()});
		}

		for (int i = 0; i < listaParticiones.size(); i++) {
			Particion particion = listaParticiones.get(i);
			modeloParticiones.addRow(new String[] {Integer.toString(i + 1),
				particion.getLocalidad() + "K", 
				particion.getTamanioDeParticion() + "K", 
				particion.getEstadoDeParticion().toString(),
				particion.getIdDelProceso()
			});
		}

	}

	private void inicializacion() {

		modeloProcesos = new DefaultTableModel(cabeceraTablaProcesos, 0);
		modeloAreaLibre = new DefaultTableModel(cabeceraTablaAreaLibre, 0);
		modeloParticiones = new DefaultTableModel(cabeceraTablaParticiones, 0);

		//inicializacion inicial de tabla de procesos
		listaProcesos = new ArrayList<>();
		listaAreaLibre = new ArrayList<>();
		listaParticiones = new ArrayList<>();

		listaProcesos.add(new Proceso("A",  8, 1, 7));
		listaProcesos.add(new Proceso("B", 14, 2, 7));
		listaProcesos.add(new Proceso("C", 18, 3, 4));
		listaProcesos.add(new Proceso("D",  6, 4, 6));
		listaProcesos.add(new Proceso("E", 14, 5, 5));

		for (Proceso p : listaProcesos) {
			modeloProcesos.addRow(new String[] {
				p.getIdDelProceso(), 
					p.getTamanioDelProceso() + "K", 
					p.getTiempoDeLlegada() + "", 
					p.getDuracionDelProceso() + ""});
		}

		listaAreaLibre.add(new AreaLibre(totalMemoriaSo, totalMemoria-totalMemoriaSo, Estado.Disponible));
		
		for (int i = 0; i < listaAreaLibre.size(); i++) {
			AreaLibre area = listaAreaLibre.get(i);
			modeloAreaLibre.addRow(new String[] { 
				Integer.toString(i+1),
					area.getLocalidad() + "K", 
					area.getTamanioDeAreaLibre() + "K", 
					Estado.Disponible.toString()});
		}

		botonControlSimulacion = vista.getBotonControlSimulacion();
		tablaProcesos = vista.getTablaProcesos();
		tablaAreaLibre = vista.getTablaAreaLibre();
		tablaParticiones = vista.getTablaParticiones();
		panelGrafico = vista.getPanelGrafica();

		tablaProcesos.setModel(modeloProcesos);
		tablaAreaLibre.setModel(modeloAreaLibre);
		tablaParticiones.setModel(modeloParticiones);

		botonControlSimulacion.setText("Paso " + this.paso);
		botonControlSimulacion.addActionListener(new AccionBoton());

	}

	private class AccionBoton implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			siguientePasoSimulador();
		}

	}

}
