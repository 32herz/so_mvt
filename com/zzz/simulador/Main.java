package com.zzz.simulador;

public class Main {
	public static void main(String[] args) {
		GUI ventana = new GUI();
		try {
			ControladorSimulacion controlador = new ControladorSimulacion(ventana);
			ventana.mostrarVentana();
		} catch (Exception e) {
			System.out.println(e);
			System.exit(-1);
		}
	}
}
