package com.zzz.simulador;

public class Particion {

	private int localidad;
	private int tamanioParticion;
	private Estado estadoParticion;
	private String idProceso;

	public Particion(int localidad, int tamanioParticion, Estado estadoParticion, String idProceso) {
		setLocalidad(localidad);
		setTamanioDeParticion(tamanioParticion);
		setEstadoDeParticion(estadoParticion);
		setIdDelProceso(idProceso);
	}

	public int getLocalidad() {
		return this.localidad;
	}

	public int getTamanioDeParticion() {
		return this.tamanioParticion;
	}

	public Estado getEstadoDeParticion() {
		return this.estadoParticion;
	}

	public String getIdDelProceso() {
		return this.idProceso;
	}

	public void setLocalidad(int localidad) {
		this.localidad = Math.max(0, localidad);
	}

	public void setTamanioDeParticion(int tamanioParticion) {
		this.tamanioParticion = Math.max(0, tamanioParticion);
	}

	public void setEstadoDeParticion(Estado estadoParticion) {
		this.estadoParticion = estadoParticion;
	}

	public void setIdDelProceso(String idProceso) {
		this.idProceso = idProceso;
	}

	@Override
	public String toString() {
		return String.format("[Localidad=%dK, Tamaño=%dK, Estado=%s, Proceso=%s]",
				getLocalidad(), 
				getTamanioDeParticion(), 
				getEstadoDeParticion(), 
				getIdDelProceso());
	}

}
